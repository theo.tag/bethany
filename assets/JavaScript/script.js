
$(document).ready(function(){

    $('.owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:2,
                dotsEach: 1
            }
        }
    })
     
    $('.grid').isotope({
        // options
        itemSelector: '.element-item',
        layoutMode: 'fitRows'
    });
 
  $('#iso button').on("click", function (){
    let value = $(this).attr('data-name')
    $('.grid').isotope({
        filter : value
      })
  })
  $('.count').countTo();
  if ($(window).scrollTop() < 100) {
        $('#back').fadeOut()
    }else {
        $('#back').fadeIn();
    }
  if (window.matchMedia("(min-width: 900px)").matches) {
    $(window).scroll(function() {
        // If on top fade the bouton out, else fade it in
        if ( $(window).scrollTop()  < 100 ){
            $('#nav').css('margin-top','1%')
            $('#nav').removeClass('mx-0','px-0','container-fluid')
            $('nav').removeClass('px-8')
            $('#nav').addClass('container')
            $('.container-lg').css('max-width','1320px')
    
        }else {
            $('#nav').css('margin-top','0%')
            $('#nav').addClass('mx-0','px-0','container-fluid')
            $('#nav').removeClass('container')
            $('nav').addClass('px-8')   
            $('.container-lg').css('max-width','2000px')
        }
    });
  } else {
    $('#nav').css('margin-top','0%')
    }

    $('.navbar-toggler-icon i').on('click',function () {
        if ( $('.navbar-toggler-icon i').hasClass('fa-bars')) {
            $('.navbar-toggler-icon i').removeClass('fa-bars')
            $('.navbar-toggler-icon i').addClass('fa-xmark')
        }else{
        $('.navbar-toggler-icon i').removeClass('fa-xmark')
        $('.navbar-toggler-icon i').addClass('fa-bars')
        }
    })

    $('#iso button').on('click', function () {
        $('#iso button').removeClass('green-color text-white')
        $('#iso button').removeClass('text-white')
        $('#iso button').addClass('bg-white')
        $(this).addClass('text-white green-color')
        $(this).removeClass('bg-white')
    })

    $('#nav li').on('click', function () {   
        $('nav li a').css('color','black')
        $(this).find('a').css('color','#319970')          
    })

    $('#home').on({

        mouseenter: function () {

            $('#home1').css('color', '#319970')

        },

        mouseleave: function () {

            $('#home1').css('color', 'black')

        }

    });

    $('#about').on({

        mouseenter: function () {

            $('#about1').css('color', '#319970')

        },

        mouseleave: function () {

            $('#about1').css('color', 'black')

        }

    });

    $('#services').on({

        mouseenter: function () {

            $('#services1').css('color', '#319970')

        },

        mouseleave: function () {

            $('#services1').css('color', 'black')

        }

    });

    $('#portfolio').on({

        mouseenter: function () {

            $('#portfolio1').css('color', '#319970')

        },

        mouseleave: function () {

            $('#portfolio1').css('color', 'black')

        }

    });

    $('#team').on({

        mouseenter: function () {

            $('#team1').css('color', '#319970')

        },

        mouseleave: function () {

            $('#team1').css('color', 'black')

        }

    });

    $(".owl-dots").append('<div class="owl-dot"><span></span></div>')
});


